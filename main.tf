
terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "1.23.0"
    }
  }
}

variable "hcloud_token" {}

provider "hcloud" {
  token = var.hcloud_token
}


module "cluster" {
  source  = "cicdteam/k3s/hcloud"
  version = "0.1.1"
  hcloud_token = var.hcloud_token
  
  datacenter = "nbg1"
  ssh_keys = ["MacBook-Pro.local"]

  master_type = "cx21"

  node_groups = {
    "cx21" = 2
  }
}

output "master_ipv4" {
  depends_on  = [module.cluster]
  description = "Public IP Address of the master node"
  value       = module.cluster.master_ipv4
}

output "nodes_ipv4" {
  depends_on  = [module.cluster]
  description = "Public IP Address of the worker nodes"
  value       = module.cluster.nodes_ipv4
}
